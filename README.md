# exchange-backend

## Описание
Биржа для обмена валют.

Биржа использует в своей работе запущенные приложения крипто-кошельки Ethereum, Ethereum Classic, BTC, BCC и др. для мониторинга статуса поступления средств извне, операций перевода поступающих средств от пользователей на горячих кошелек и операций вывода средств на кошельки пользователей с горячего кошелька. 

Для каждого кошелька в проекте заводится Dockerfile для сборки docker image.


## Installation

```bash
$ yarn install

$ docker-compose build

$ docker-compose up -d
```

Сервис будет доступен по адресу: http://127.0.0.1:4000/api/v1

Swagger: http://127.0.0.1:4000/swagger

## Диаграмма, как я вижу дальнейшее разделение на сервисы

![diagram](https://gitlab.com/flasher007/exchange-backend/raw/master/exchange.png)
