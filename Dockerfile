FROM node:10.15.3-alpine
RUN npm install -g yarn node-gyp typescript ts-node @nestjs/cli 
WORKDIR /app
COPY . .
RUN yarn install
EXPOSE 4000
CMD [ "yarn", "run", "start"]