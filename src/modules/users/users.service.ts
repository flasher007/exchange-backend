import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entity/user.entity';
import { iUser } from './interface/user.interface';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  // async findByEmail(email: string): Promise<User> {
  //     return await this.userRepository.findOne({
  //         where: {
  //             email,
  //         }
  //     });
  // }

  // async findById(id: number): Promise<User> {
  //     return await this.userRepository.findOne({
  //         where: {
  //             id,
  //         }
  //     });
  // }

  async create(data: iUser): Promise<User> {
    const user = this.userRepository.create();

    user.email = data.email;
    user.password = data.password;

    return await this.userRepository.save(user);
  }
}
