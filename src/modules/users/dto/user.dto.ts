import { IsEmail, IsNotEmpty, MinLength, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUserDto {
    @IsEmail({}, { message: 'Email is invalid' })
    @ApiModelProperty({ required: true })
    readonly email: string;

    @IsNotEmpty({ message: "Password is required" })
    @MinLength(6)
    @MaxLength(32)
    @ApiModelProperty({ required: true })
    readonly password: string;
}