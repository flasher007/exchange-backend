import {
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Get,
  Request,
  Response,
  Body,
  HttpException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ApiUseTags } from '@nestjs/swagger';
import { CreateUserDto } from '../users/dto/user.dto';

@ApiUseTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('/login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() data: CreateUserDto): Promise<any> {
    const { email, password } = data;

    return await this.authService.signIn(email, password);
  }

  @Post('/registration')
  async registration(@Request() req): Promise<any> {
    const { email, password } = req.body;

    return await this.authService.signUp(email, password);
  }

  @Post('/refresh-token')
  async refreshToken(@Request() req): Promise<any> {
    const { refreshToken } = req.body;

    return await this.authService.refreshToken(refreshToken);
  }

}
