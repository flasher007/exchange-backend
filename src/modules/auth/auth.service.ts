import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload, Token } from './interface/jwt-payload.interface';
// import { User } from '../users/entity/user.entity';
import { iUser } from '../users/interface/user.interface';

@Injectable()
export class AuthService {
  tokenLife = parseInt(process.env.AUTH_TOKENLIFE, 10) || 3000;
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async signIn(email, password): Promise<Token> {
    const user: JwtPayload = { email };
    return this.generateTokens(user);
  }

  async signUp(email, password): Promise<any> {
    const user: iUser = { email, password };
    return await this.usersService.create({ email, password });
  }

  generateTokens(user) {
    return {
      token_type: 'bearer',
      access_token: this.generateAccessToken(user),
      expires_in: this.tokenLife,
      refresh_token: this.generateRefreshToken(user),
    };
  }

  generateAccessToken(user) {
    return this.jwtService.sign(user, { expiresIn: this.tokenLife });
  }

  generateRefreshToken(user) {
    return this.jwtService.sign(user, { expiresIn: '1y' });
  }

  async refreshToken(token: string): Promise<any> {
    const user: iUser = await this.jwtService.verify(token);
    // const tokens = await this.jwtService.generateToken(user);

    return this.generateTokens(user);
  }
}
