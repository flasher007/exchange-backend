import { IsEmail, IsNotEmpty, MinLength, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class TokenDto {
  @ApiModelProperty({ required: true })
  readonly access_token: string;

  @ApiModelProperty({ required: true })
  readonly refresh_token: string;

  @ApiModelProperty({ required: true })
  readonly expires_in: number;
}
