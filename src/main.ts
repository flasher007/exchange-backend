import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  const PORT = process.env.PORT || 4000;
  const HOSTNAME = process.env.HOSTNAME || '127.0.0.1';
  const PREFIX_URL = process.env.PREFIX_URL || 'api/v1';

  app.use(helmet());
  app.enableCors();
  app.setGlobalPrefix(PREFIX_URL);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  const options = new DocumentBuilder()
    .setTitle('Exchange Back API')
    .setDescription('Exchange API description')
    .setBasePath(PREFIX_URL)
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(PORT, HOSTNAME);
}
bootstrap();
