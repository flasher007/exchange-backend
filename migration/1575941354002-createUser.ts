import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUser1575941354002 implements MigrationInterface {
  name = 'createUser1575941354002';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "users" (
          "id" SERIAL NOT NULL, 
          "email" character varying(500) NOT NULL, 
          "password" character varying NOT NULL, 
          "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
          "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
          CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id")
        )`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "users"`, undefined);
  }
}
